#NAWK

##Usage
cat some/file.txt | nawk [<token> ...]
##Example 
cat some/file.txt | nawk 0 -1

##Description
Nawk prints the corresponding tokens of each line in the file

- negative numbers are relative to the end of the line
- 0 is the first element in the line
- -1 is the last element in the line 
- -2 is the second to last 

cat some/file.txt | nawk -1 0
the above line prints the last and first element in the line.
