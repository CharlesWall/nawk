#!/usr/bin/env node

const fs = require('fs')

const readline = require('readline')
const Promise = require('bluebird')

const DELIMITER = ' '
let args = process.argv.slice(2)

const indexes = []
let streams = []

for(let i = 0; i < args.length; i++) {
  let arg = args[i]

  if(!Number.isNaN(+arg)) {
    indexes.push(arg)
  } else {
    switch(arg) {
      case '-':
        streams.push(process.stdin)
        break
      case '-f': case '--file':
        streams.push(fs.createReadStream(args[++i]))
        break
      default:
        console.error('invalid argument:', arg)
        process.exit(0)
        break
    }
  }
}

function handleLine(line) {
  let message = ''

  let tokens = line.split(/\s+/)

  indexes.forEach(function(index){
    index = +index
    let val = null

    if(index >= 0){
      val = tokens[index]
    } else {
      val = tokens[tokens.length + index]
    }

    if(val){
      message += val + DELIMITER
    }

  })
  if(message){
    process.stdout.write(message + '\n')
  }
}

let promise = Promise.resolve()

if (!streams.length) {
    streams = [process.stdin];
}

streams.forEach(function(stream) {
  promise = promise.then(function() {
    return new Promise((resolve, reject) => {

      readline.createInterface({
        input: stream,
        output: process.stdout,
        terminal: false
      }).on('line', handleLine)

      stream.on('end', resolve)
      stream.on('error', reject)
    })
  })
})
